package features;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

/**
 * Created by valdiane.santos on 10/05/2017.
 */
public class TestsDriver {

    private AndroidDriver driver;

    public TestsDriver() throws Exception {
        this.setUpDriver();
    }

    private void setUpDriver() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
        capabilities.setCapability(MobileCapabilityType.APP, AllTest.appPath);
        this.driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
    }
}
