# language: pt

Funcionalidade: Enviar mensagens


  Esquema do Cenario: Enviar uma mensagem em uma nova conversa
    Dado que eu esteja <status>
    E que eu clique no icone do WhatsApp
    E que eu selecione um contato cadastrado
    E que eu escreva uma mensagem
    Quando eu clicar em enviar
    Entao a mensagem <envio>

    Exemplos:
      | status  | envio                    |
      | online  | sera enviada             |
      | offline | ficara pendente de envio |


  Esquema do Cenario: Enviar uma mensagem em uma conversa existente
    Dado que eu esteja <status>
    E que eu clique no icone do WhatsApp
    E que eu selecione uma conversa existente
    E que eu escreva uma mensagem
    Quando eu clicar em enviar
    Entao a mensagem <envio> enviada

    Exemplos:
      | status  | envio                    |
      | online  | sera enviada             |
      | offline | ficara pendente de envio |



