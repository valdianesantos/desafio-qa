package features;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by valdiane.santos on 10/05/2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "steps_definitions",
        features = "classpath:feature_files",
        format = "json:target/cucumber.json",
        tags = {"~@ignore"},
        strict = true,
        monochrome = true)

public class AllTest {
    public static TestsDriver driver;
    public  static String appPath;
}
