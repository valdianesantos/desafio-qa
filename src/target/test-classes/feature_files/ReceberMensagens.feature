# language: pt

Funcionalidade: Receber mensagens

  Cenario: Ler mensagem recebida
    Dado que eu esteja online
    E que eu receba uma mensagem com o conteudo "teste de recebimento"
    E que eu clique no icone do WhatsApp
    Quando eu clicar no contato que enviou a mensagem
    Entao eu lerei o conteudo "teste de recebimento"
