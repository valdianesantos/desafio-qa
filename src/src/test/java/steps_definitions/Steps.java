package steps_definitions;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

/**
 * Created by valdiane.santos on 10/05/2017.
 */
public class Steps {

    @Dado("^que eu esteja (.+)$")
    public void que_eu_esteja_online() throws Throwable {
        throw new PendingException();
    }

    @Dado("^que eu clique no icone do WhatsApp$")
    public void que_eu_clique_no_icone_do_WhatsApp() throws Throwable {
        throw new PendingException();
    }

    @Dado("^que eu selecione um contato cadastrado$")
    public void que_eu_selecione_um_contato_cadastrado() throws Throwable {
        throw new PendingException();
    }

    @Dado("^que eu escreva uma mensagem$")
    public void que_eu_escreva_uma_mensagem() throws Throwable {
        throw new PendingException();
    }

    @Quando("^eu clicar em enviar$")
    public void eu_clicar_em_enviar() throws Throwable {
        throw new PendingException();
    }

    @Entao("^a mensagem (.+)$")
    public void a_mensagem_sera_enviada() throws Throwable {
        throw new PendingException();
    }

    @E("^que eu selecione uma conversa existente$")
    public void queEuSelecioneUmaConversaExistente() throws Throwable {
        throw new PendingException();
    }

    @E("^que eu receba uma mensagem com o conteudo \"([^\"]*)\"$")
    public void queEuRecebaUmaMensagemComOConteudo(String arg0) throws Throwable {
        throw new PendingException();
    }

    @Quando("^eu clicar no contato que enviou a mensagem$")
    public void euClicarNoContatoQueEnviouAMensagem() throws Throwable {
        throw new PendingException();
    }

    @Entao("^eu lerei o conteudo \"([^\"]*)\"$")
    public void euLereiOConteudo(String arg0) throws Throwable {
        throw new PendingException();
    }
}
